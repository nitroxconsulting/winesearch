package helpers;

import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.pdfclown.util.StringUtils;

public class NxFormat {
	static NumberFormat formatter=java.text.NumberFormat.getInstance(java.util.Locale.ENGLISH);
	static NumberFormat d=new DecimalFormat("#0.####");
	static NumberFormat amt=new DecimalFormat("### ### ### ##0");
	
	public static String dbl(double d){
		if(d==0)
			return "-";
		formatter=new DecimalFormat("#0.####");
		return formatter.format(d).replace(',', '.');
	}
	public static String dbl(double d, int nDec){
		if(d==0)
			return "-";
		String format="#0."+StringUtils.repeat("#", nDec);
		return dbl(d, format);
	}
	public static String dbl(double d, String format){
		if(d==0)
			return "-";
		formatter=new DecimalFormat(format);
		return formatter.format(d).replace(',', '.');
	}
	
	public static String amt(int amt){
		formatter = new DecimalFormat("#,###");
		return formatter.format(amt);
	}
	public static String date(Date d, String format){
		SimpleDateFormat f = new SimpleDateFormat(format);
		return f.format(d);
	}
	public static String datetime(long t, String format){
		Date d=new Date(t);
		SimpleDateFormat f = new SimpleDateFormat(format);
		return f.format(d);
	}
	public static java.util.Date parseDate(String sDate, String format) throws ParseException{
		SimpleDateFormat f = new SimpleDateFormat(format);
		return f.parse(sDate);
	}
	public static String getNumeric(String s){
		String ret="";
		for(int i=0;i<s.length();i++){
			char c=s.charAt(i);
			if(isNumeric(c))
				ret+=c;
		}
		return ret;
	}
	static boolean isNumeric(char c){
		boolean ret;
		ret = (c==46 ); // (dot)
		ret = ret || (c>47 && c<58);
		return ret;
	}
	public static String stripAccents(String str) 
	{
	    
	    String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD); 
	    Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
	    return pattern.matcher(nfdNormalizedString).replaceAll("");
	}
}