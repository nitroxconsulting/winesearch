package nx.winesearch.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import nx.winesearch.models.*;



@Repository("CalibDao")  
@Transactional
public class CalibDaoImpl extends AbstractDao {

	public Calib getCalib(int wineId, int idccy) {
		Criteria criteria= getSession().createCriteria(Calib.class)
				.add(Restrictions.eq("wine.id", wineId)).add(Restrictions.eq("ccy", idccy)).setMaxResults(1);
		return (Calib) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public static List<Calib> getSearchedCalib(int wineId, Date startDate, Date endDate)
	{
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date newStartDate = new Date();
		Date newEndDate = new Date();
		
		try {
			newStartDate = df.parse(startDate.toString());
			newEndDate = df.parse(endDate.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Criteria criteria = getSession().createCriteria(Calib.class)
				.add(Restrictions.and(Restrictions.le("date", newEndDate),Restrictions.ge("date", newStartDate)))
				.add(Restrictions.eq("wine.id", wineId));
		
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	public static List<Calib> getSearchedCalibByCCY(int ccy, Date d)
	{			
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date newDate = new Date();
		
		try {
			newDate = df.parse(d.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		Criteria criteria = getSession().createCriteria(Calib.class)
				.add(Restrictions.and(Restrictions.eq("date", newDate), Restrictions.eq("ccy", ccy)));
		
		return criteria.list();
	}
	
	public Calib getCalibRecentVintages(int wineId, Date d, int idccy)
	{
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date newDate = new Date();
		
		try {
			newDate = df.parse(d.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Criteria criteria = getSession().createCriteria(Calib.class)
				.add(Restrictions.eq("wine.id", wineId))
				.add(Restrictions.gt("date", newDate))
				.add(Restrictions.eq("ccy", idccy))
				.addOrder(Order.desc("date"))
				.setMaxResults(1);
		
		return (Calib) criteria.uniqueResult();
		
	}

}
