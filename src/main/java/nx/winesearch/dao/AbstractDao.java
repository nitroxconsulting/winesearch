package nx.winesearch.dao;

import java.io.Serializable;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

@SuppressWarnings("deprecation")
public abstract class AbstractDao{
	
	static Session session;
	
		public static void openSession(){
			Configuration configuration = new Configuration();
	        configuration.configure("/lib_and_config/hibernate.cfg.xml");
	        ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
	        SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	        session = sessionFactory.openSession();
		}
	
		public static void CloseSession(){
			getSession().close();
		}
	
		protected static Session getSession(){
          return session;
	   }

	   public static Serializable persist(Object entity) {
		   session.getTransaction().begin();
		   Serializable ret= session.save(entity);
		   session.getTransaction().commit();
		   return ret;
	   }
	    
	   public static void delete(Object entity) {   
		   session.delete(entity);
	   }
	   
	   public static void edit(Object entity){
		   session.saveOrUpdate(entity);
	   }
	   
}
