package nx.winesearch.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import nx.winesearch.models.*;

@Repository("singleWineDao")
public class SingleWineImpl extends AbstractDao {
	
	public SingleWine getPrices(int idcalib, int year)
	{
		Criteria criteria = getSession().createCriteria(SingleWine.class)
				.add(Restrictions.eq("calib.id", idcalib))
				.add(Restrictions.eq("year", year));
		
		return (SingleWine) criteria.uniqueResult();
	}

}
