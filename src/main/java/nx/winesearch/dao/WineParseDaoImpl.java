package nx.winesearch.dao;
import java.util.List;

import nx.winesearch.main.WineParsing;
import nx.winesearch.models.WineParse;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("wineParseDao")
public class WineParseDaoImpl extends AbstractDao {
	
	@SuppressWarnings("unchecked")
	public static List<WineParse> getWineParseDiscard(){
		
		Criteria criteria = getSession().createCriteria(WineParse.class)
				.add(Restrictions.eq("state", "discard"));
		
		return (List<WineParse>)criteria.list();
		
	}	
		
		@SuppressWarnings("unchecked")
		public static List<WineParse> getWineParseNew(){
			
			Criteria criteria = getSession().createCriteria(WineParse.class)
					.add(Restrictions.eq("state", "new"));
			
			return (List<WineParse>)criteria.list();
			
		}
			
	public static void updateWineParse(WineParse wine) {
		System.out.println(wine.getId()+" | "+wine.getParse()+" | "+wine.getPotname2()+" | "+wine.getScore2());
		try {
			getSession().getTransaction().begin();
			getSession().update(wine);
						
//			getSession().createQuery("UPDATE WineParse Set potid2= :potid2, potname2= :potname2, score2= :score2 where id= :id")
//	    	.setInteger("potid2", wine.getPotid2()).setString("potname2", wine.getPotname2())
//	    	.setDouble("score2", wine.getScore2()).setInteger("id", wine.getId()).executeUpdate();
			
			getSession().getTransaction().commit();
		} catch (Exception e) {
			getSession().getTransaction().rollback();
			e.printStackTrace();
		}
	}
	
}
