package nx.winesearch.dao;

import java.io.Serializable;



import nx.winesearch.models.WineSearch;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("wineSearchDao")
@Transactional
public class WineSearchDaoImpl extends AbstractDao {
	
	public static Serializable addWineSearch(WineSearch ws)
	{
		return persist(ws);		
	}

}
