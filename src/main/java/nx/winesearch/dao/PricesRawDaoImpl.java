package nx.winesearch.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import nx.winesearch.models.*;


@Repository("priceRawDao")  
@Transactional
public class PricesRawDaoImpl extends AbstractDao {
	
	@SuppressWarnings("unchecked")
	public List<PricesRaw> getPrices() throws Exception
	{
		Criteria criteria = getSession().createCriteria(PricesRaw.class)
				.addOrder(Order.desc("date"))
				.setMaxResults(2000);
				
		return (List<PricesRaw>) criteria.list();
	}
	
	public Long getPricesRawCount()
	{
		Criteria criteria = getSession().createCriteria(PricesRaw.class)
				.setProjection(Projections.rowCount());
		
		return (Long)criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<PricesRaw> getPagedData(long page, long PerPage) {

		Criteria criteria = getSession().createCriteria(PricesRaw.class)
				.setFirstResult((int) ((page-1)*PerPage))
				.setMaxResults((int) PerPage);
		System.out.println("1st: "+((page-1)*PerPage)+" 2nd: "+((page*PerPage)-1));
		return (List<PricesRaw>)criteria.list();
	}
	
	public int getFirstYear(int minyear, Date date, int wineId)
	{
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date newDate = new Date();
		
		try {
			newDate = df.parse(date.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Criteria criteria = getSession().createCriteria(PricesRaw.class)
				.add(Restrictions.and(Restrictions.gt("year", minyear), Restrictions.gt("date", newDate)))
				.add(Restrictions.eq("wine.id", wineId))
				.addOrder(Order.desc("year"))
				.setProjection(Projections.distinct(Projections.property("year")))
				.setMaxResults(1);
		
		return (Integer) criteria.uniqueResult();
				
	}

	@SuppressWarnings("unchecked")
	public List<PricesRaw> getPrices(Date date1, Date date2) {
		Criteria criteria= getSession().createCriteria(PricesRaw.class).add(Restrictions.and(Restrictions.le("date", date1), Restrictions.ge("date", date2)))
				.addOrder(Order.desc("date"));
		return (List<PricesRaw>) criteria.list();
	}
	
}
