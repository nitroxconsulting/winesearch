package nx.winesearch.dao;

import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;

import java.sql.DriverManager;
import java.sql.Statement;

import nx.winesearch.models.*;

public class CurrencyDaoImpl {

	public static List<Currency> getCurrencies() throws Exception{
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn= (Connection) DriverManager.getConnection("jdbc:mysql://localhost/currencyapp?user=root&password=lepolo2127");
		Statement stmt = conn.createStatement();
		java.sql.ResultSet rs =  stmt.executeQuery("SELECT * from ccy");
		
		List<Currency> currencies= new ArrayList<Currency>();
		while(rs.next()){
			Currency currency= new Currency();
			currency.setId(rs.getInt("idccy"));
			currency.setCcy(rs.getString("CCY"));
			currency.setName(rs.getString("NAME"));
			currencies.add(currency);
		}
		
		rs.close();
		stmt.close();
		conn.close();
		return currencies;
	}

	public static Currency getCurrency(String ccy) throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn= (Connection) DriverManager.getConnection("jdbc:mysql://localhost/currencyapp?user=root&password=lepolo2127");
		Statement stmt = conn.createStatement();
		ResultSet rs =  (ResultSet) stmt.executeQuery("SELECT * from ccy where CCY LIKE '"+ccy+"'");
	

		Currency currency= new Currency();
		if(rs.next()){
			currency.setId(rs.getInt("idccy"));
			currency.setCcy(rs.getString("CCY"));
			currency.setName(rs.getString("NAME"));
		}
		
		rs.close();
		stmt.close();
		conn.close();
		return currency;
	}

	public static Currency getCurrency(int id) throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn= (Connection) DriverManager.getConnection("jdbc:mysql://localhost/currencyapp?user=root&password=lepolo2127");
		Statement stmt = conn.createStatement();
		ResultSet rs =  (ResultSet) stmt.executeQuery("SELECT * from ccy where idccy LIKE '"+id+"'");
	

		Currency currency= new Currency();
		if(rs.next()){
			currency.setId(rs.getInt("idccy"));
			currency.setCcy(rs.getString("CCY"));
			currency.setName(rs.getString("NAME"));
		}
		
		rs.close();
		stmt.close();
		conn.close();
		return currency;
	}
	


}
