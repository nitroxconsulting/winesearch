package nx.winesearch.dao;


import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import nx.winesearch.models.*;


@Repository("ValuationDao")  
@Transactional
public class ValuationDaoImpl extends AbstractDao {

	public Valuation getValuation(int idCalib, int year) {
		Criteria criteria= getSession().createCriteria(Valuation.class).add(Restrictions.eq("calib.id", idCalib)).add(Restrictions.eq("year", year))
				.setMaxResults(1);
		return (Valuation)criteria.uniqueResult();
	}

	public static Double getAvgBotPrice(int calibId)
	{
		Criteria criteria = getSession().createCriteria(Valuation.class)
				.add(Restrictions.eq("calib.id", calibId))
				.setProjection(Projections.avg("value"));
		
		return (Double)criteria.uniqueResult();
	}


}
