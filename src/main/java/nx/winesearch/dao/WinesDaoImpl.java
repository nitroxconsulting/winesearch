package nx.winesearch.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import nx.winesearch.models.*;

@Repository("wineDao")
public class WinesDaoImpl extends AbstractDao {

	@SuppressWarnings("unchecked")
	public static List<Wine> getWines()
	{
		Criteria criteria = getSession().createCriteria(Wine.class);
		
		return (List<Wine>)criteria.list();
	}
	
	public Wine getWineById(int id)
	{
		Criteria criteria = getSession().createCriteria(Wine.class)
				.add(Restrictions.eq("id", id));
		return (Wine)criteria.uniqueResult();
	}
	
	public static Wine getWineByName(String winename)
	{
		Criteria criteria = getSession().createCriteria(Wine.class)
				.add(Restrictions.eq("name", winename));
		return (Wine)criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<Wine> WineSearch(String wineName)
	{
		Criteria criteria = getSession().createCriteria(Wine.class)
				.add(Restrictions.ilike("name", '%'+wineName+'%'))
				.setMaxResults(10);
		return (List<Wine>)criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public static List<Wine> getWineByAppellation(int appellation)
	{
		Criteria criteria = getSession().createCriteria(Wine.class)
				.add(Restrictions.eq("appelation.id", appellation))
				.add(Restrictions.eq("premium", 1));
		
		return (List<Wine>)criteria.list();
	}

	
}
