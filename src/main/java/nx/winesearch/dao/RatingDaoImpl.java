package nx.winesearch.dao;


import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import nx.winesearch.models.*;

@Repository("ratingDao")
public class RatingDaoImpl extends AbstractDao {

	public static Double getRating(int wineId)
	{
		Criteria criteria = getSession().createCriteria(Rating.class)
				.add(Restrictions.eq("wine", wineId))
				.setProjection(Projections.avg("value"));
		
		return (Double) criteria.uniqueResult();
	}
	
	public Rating getRatingPerYr(int wineId, int year)
	{
		Criteria criteria = getSession().createCriteria(Rating.class)
				.add(Restrictions.eq("wine", wineId))
				.add(Restrictions.eq("year", year));
		
		return (Rating) criteria.uniqueResult();
	}
	
}
