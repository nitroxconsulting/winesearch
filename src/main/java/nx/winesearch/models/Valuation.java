package nx.winesearch.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "valuation") 
public class Valuation {
	
	@Id
	@GeneratedValue
	@Column(name = "id_valuation")
	private int id;
	
	@JoinColumn(name = "idcalib")
	@ManyToOne
	private Calib calib;
	
	@Column(name = "value", columnDefinition = "decimal(8,2)")
	private double value;
	
	@Column(name = "year")
	private int year;
	
	public Valuation(){}
	
	public Valuation(Calib idCalib, Double value, int year)
	{
		this.calib = idCalib;
		this.value = value;
		this.year = year;
	}	

	public Calib getId() {
		return calib;
	}

	public void setId(Calib id) {
		this.calib = id;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
}
