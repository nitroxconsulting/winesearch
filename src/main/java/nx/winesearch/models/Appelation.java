package nx.winesearch.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "appelation")
public class Appelation {
	
	@Id
	@GeneratedValue
	@Column(name = "idappelation")
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "idarea")
	private Area area;
	
	@Column(name = "name")
	private String name;
	
	public Appelation(){}
	
	public Appelation(Area area, String name){
		this.area = area;
		this.name = name;
	}	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
