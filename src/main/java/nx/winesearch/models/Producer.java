package nx.winesearch.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity  
@Table(name = "producer")  
public class Producer {

	@Id  
	@GeneratedValue  
	@Column(name = "idproducer")
	private int id;
	
	@Column(name = "name")
	private String name;
	
	public Producer(){}
	
	public Producer(String name)
	{
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
