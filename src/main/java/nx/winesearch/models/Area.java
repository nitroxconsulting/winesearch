package nx.winesearch.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "area")  
public class Area {

	@Id
	@GeneratedValue
	@Column(name = "idarea")
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "idcountry")
	private Country country;
		
	@Column(name = "name")
	private String name;
	
	public Area(){}
	
	public Area(Country country, String name){
		this.country = country;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
