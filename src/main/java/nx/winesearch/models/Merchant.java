package nx.winesearch.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity  
@Table(name = "merchant")
public class Merchant {

	@Id  
	@GeneratedValue  
	@Column(name = "idmerchant")
	private int id;
	
	@Column(name = "name")
	private String name;
	
	public Merchant(){}
	
	public Merchant(String name)
	{
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
