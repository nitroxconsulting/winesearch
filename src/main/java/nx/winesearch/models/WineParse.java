package nx.winesearch.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity  
@Table(name = "wine_parse")  
public class WineParse {
	
	@Id
	@GeneratedValue
	@Column(name = "idparse")
	private int id;
	
	@Column(name = "parse")
	private String parse;
	
	@Column(name = "idwine")
	private Integer wine;

	@Column(name = "idcolor")
	private Integer color;
	
	@Column(name = "potid")
	private Integer potid;
	
	@Column(name = "score", columnDefinition="decimal(8,2)")
	private double score;
	
	@Column(name = "potname")
	private String potname;
	
	@Column(name = "potid_2")
	private Integer potid2;
	
	@Column(name = "score_2", columnDefinition="decimal(8,2)")
	private double score2;
	
	@Column(name = "potname_2")
	private String potname2;
	
	@Column(name = "year")
	private Integer year;
	
	@Column(name = "price", columnDefinition="decimal(8,0)")
	private double price;
	
	@Column(name = "idccy")
	private Integer ccy;

	@Column(name = "idmerchant")
	private Integer merchant;
	
	@Column(name="state")
	private String state;
	
	public WineParse(){}
	
	public WineParse(String parse, Integer idWine, Integer idColor, Integer potId, Double score,
					 String potname, Integer year, Double price, Integer idCcy, Integer merchant)
	{
		this.parse = parse;
		this.wine = idWine;
		this.color = idColor;
		this.potid = potId;
		this.score = score;
		this.potname = potname;
		this.year = year;
		this.price = price;
		this.ccy = idCcy;
		this.merchant = merchant;
	}	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getParse() {
		return parse;
	}

	public void setParse(String parse) {
		this.parse = parse;
	}

	public Integer getWine() {
		return wine;
	}

	public void setWine(Integer wine) {
		this.wine = wine;
	}

	public Integer getColor() {
		return color;
	}

	public void setColor(Integer color) {
		this.color = color;
	}

	public Integer getPotid() {
		return potid;
	}

	public void setPotid(Integer potid) {
		this.potid = potid;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public String getPotname() {
		return potname;
	}

	public void setPotname(String potname) {
		this.potname = potname;
	}
	
	public Integer getPotid2() {
		return potid2;
	}

	public void setPotid2(Integer potid2) {
		this.potid2 = potid2;
	}

	public double getScore2() {
		return score2;
	}

	public void setScore2(double score2) {
		this.score2 = score2;
	}

	public String getPotname2() {
		return potname2;
	}

	public void setPotname2(String potname2) {
		this.potname2 = potname2;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Integer getCcy() {
		return ccy;
	}

	public void setCcy(Integer ccy) {
		this.ccy = ccy;
	}

	public Integer getMerchant() {
		return merchant;
	}

	public void setMerchant(Integer merchant) {
		this.merchant = merchant;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
}
