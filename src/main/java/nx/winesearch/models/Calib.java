package nx.winesearch.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "calib")
public class Calib {

	@Id
	@GeneratedValue
	@Column(name = "idcalib")
	private int id;
	
	
	@JoinColumn(name = "idwine")
	@ManyToOne
	private Wine wine;
		
	@Column(name = "date")
	private Date date;
	
	@Column(name = "alpha")
	private float alpha;
	
	@Column(name = "ceiling")
	private float ceiling;
	
	@Column(name = "lambda")
	private float lambda;
	
	@Column(name = "rate")
	private float rate;
	
	@Column(name = "coeffp")
	private float coeffp;
	
	@Column(name = "y0")
	private int yO;
	
	@Column(name = "quality")
	private int quality;

	@Column(name = "idccy")
	private int ccy;
	
	public Calib(){}
	
	public Calib(Wine idWine, Date date, float alpha, float ceiling,
				 float lambda, float rate, float coeffp, int y0,
				 int quality, int idccy)
	{
		this.wine = idWine;
		this.date = date;
		this.alpha = alpha;
		this.ceiling = ceiling;
		this.lambda = lambda;
		this.rate = rate;
		this.coeffp = coeffp;
		this.yO = y0;
		this.quality = quality;
		this.ccy = idccy;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Wine getWine() {
		return wine;
	}

	public void setWine(Wine wine) {
		this.wine = wine;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getAlpha() {
		return alpha;
	}

	public void setAlpha(float alpha) {
		this.alpha = alpha;
	}

	public double getCeiling() {
		return ceiling;
	}

	public void setCeiling(float ceiling) {
		this.ceiling = ceiling;
	}

	public double getLambda() {
		return lambda;
	}

	public void setLambda(float lambda) {
		this.lambda = lambda;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(float rate) {
		this.rate = rate;
	}

	public double getCoeffp() {
		return coeffp;
	}

	public void setCoeffp(float coeffp) {
		this.coeffp = coeffp;
	}

	public int getyO() {
		return yO;
	}

	public void setyO(int yO) {
		this.yO = yO;
	}

	public int getQuality() {
		return quality;
	}

	public void setQuality(int quality) {
		this.quality = quality;
	}

	public int getCcy() {
		return ccy;
	}

	public void setCcy(int ccy) {
		this.ccy = ccy;
	}

}
