package nx.winesearch.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity  
@Table(name = "rating") 
public class Rating {
	
	@Id  
	@GeneratedValue  
	@Column(name = "idrating")
	private int id;
	
	@Column(name = "idwine")
	private int wine;
	
	@Column(name = "value", columnDefinition ="decimal(8,2)")
	private double value;
	
	@Column(name = "year")
	private int year;
	
	public Rating(){}
	
	public Rating(int idWine, Double value, int year)
	{
		this.wine = idWine;
		this.value = value;
		this.year = year;
	}	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getWine() {
		return wine;
	}

	public void setWine(int wine) {
		this.wine = wine;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	
}
