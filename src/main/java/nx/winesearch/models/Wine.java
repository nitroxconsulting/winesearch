package nx.winesearch.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "wine")
public class Wine {
	
	@Id 
	@GeneratedValue
	@Column(name = "idwine")
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "idproducer")
	private Producer producer;
	
	@ManyToOne
	@JoinColumn(name = "idappelation")
	private Appelation appelation;
	
	@ManyToOne
	@JoinColumn(name = "idcolor")
	private Color color;

	@ManyToOne
	@JoinColumn(name = "idcategory")
	private Category category;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "premium", columnDefinition="BIT")
	private int premium;
	
	
	public Wine(){}
	
	public Wine(Producer prod, Appelation appel, Color col, Category categ,
				String name, int premium)
	{
		this.producer = prod;
		this.appelation = appel;
		this.color = col;
		this.category = categ;
		this.name = name;
		this.premium = premium;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Producer getProducer() {
		return producer;
	}


	public void setProducer(Producer prod) {
		this.producer = prod;
	}


	public Appelation getAppelation() {
		return appelation;
	}


	public void setAppelation(Appelation appel) {
		this.appelation = appel;
	}


	public Color getColor() {
		return color;
	}


	public void setColor(Color col) {
		this.color = col;
	}


	public Category getCategory() {
		return category;
	}


	public void setCategory(Category categ) {
		this.category = categ;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getPremium() {
		return premium;
	}


	public void setPremium(int premium) {
		this.premium = premium;
	}
	
}
