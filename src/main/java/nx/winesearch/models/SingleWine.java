package nx.winesearch.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "singlewine") 
public class SingleWine {
	
	@Id
	private int SingleWine_id;
		
	@JoinColumn(name = "idcalib")
	@ManyToOne
	private Calib calib;
	
	@Column(name = "year")
	private int year;
	
	@Column(name = "bestprice", columnDefinition = "decimal(8,1)")
	private double bestprice;
	
	@Column(name = "avgprice", columnDefinition = "decimal(8,1)")
	private double avgprice;
	
	@Column(name = "nbprice")
	private int price;
	
	public SingleWine(){}
	
	public SingleWine(Calib idcalib, int year, Double bestPrice, Double avgPrice,
					  int nbPrice)
	{
		this.calib = idcalib;
		this.year = year;
		this.bestprice = bestPrice;
		this.avgprice = avgPrice;
		this.price = nbPrice;
	}

	public Calib getId() {
		return calib;
	}

	public void setId(Calib id) {
		this.calib = id;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public double getBestprice() {
		return bestprice;
	}

	public void setBestprice(double bestprice) {
		this.bestprice = bestprice;
	}

	public double getAvgprice() {
		return avgprice;
	}

	public void setAvgprice(double avgprice) {
		this.avgprice = avgprice;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	
}
