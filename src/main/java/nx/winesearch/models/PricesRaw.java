package nx.winesearch.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "prices_raw")
public class PricesRaw {
	
	@Id
	@GeneratedValue
	@Column(name = "price_raw_id", columnDefinition = "bigint(20)")
	private int id;

	@Column(name = "date")
	private Date date;
	
	@Column(name = "year")
	private int year;
	
	@Column(name = "size")
	private int size;
	
	@Column(name = "stock")
	private int stock;
	
	@Column(name = "price", columnDefinition = "decimal(8,2)")
	private double price;
	
	@Column(name = "idccy")
	private int ccy;

	@Column(name = "status")
	private Integer status;
	
	@JoinColumn(name = "idwine")
	@ManyToOne
	private Wine wine;
	
	@ManyToOne
	@JoinColumn(name = "idmerchant")
	private Merchant merchant;
	
	public PricesRaw(){}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getCcy() {
		return ccy;
	}

	public void setCcy(int ccy) {
		this.ccy = ccy;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Wine getWine() {
		return wine;
	}

	public void setWine(Wine wine) {
		this.wine = wine;
	}

	public Merchant getMerchant() {
		return merchant;
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
