package nx.winesearch.models;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="winesearch")
public class WineSearch {
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private int id;
	
	@Column(name="idwine")
	private int idwine;
	
	@Column(name="avgparkerscore", columnDefinition="decimal(8,2)")
	private Double avgParkerScore;
	
	@Column(name="avgappellation", columnDefinition="decimal(8,2)")
	private Double avgAppellation;
	
	@Column(name="diff_1", columnDefinition="decimal(8,2)")
	private Double diff_1;
	
	@Column(name="avgbottleprice", columnDefinition="decimal(8,2)")
	private Double avgbottleprice;
	
	@Column(name="avgbottleappellation", columnDefinition="decimal(8,2)")
	private Double avgbottleappellation;
	
	@Column(name="diff_2", columnDefinition="decimal(8,2)")
	private Double diff_2;
	
	@Column(name="currency")
	private String currency;
	
	@Column(name="date_added")
	private Date date_added;
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdwine() {
		return idwine;
	}

	public void setIdwine(int idwine) {
		this.idwine = idwine;
	}

	public Double getAvgParkerScore() {
		return avgParkerScore;
	}

	public void setAvgParkerScore(Double avgParkerScore) {
		this.avgParkerScore = avgParkerScore;
	}

	public Double getAvgAppellation() {
		return avgAppellation;
	}

	public void setAvgAppellation(Double avgAppellation) {
		this.avgAppellation = avgAppellation;
	}

	public Double getDiff_1() {
		return diff_1;
	}

	public void setDiff_1(Double diff_1) {
		this.diff_1 = diff_1;
	}

	public Double getAvgbottleprice() {
		return avgbottleprice;
	}

	public void setAvgbottleprice(Double avgbottleprice) {
		this.avgbottleprice = avgbottleprice;
	}

	public Double getAvgbottleappellation() {
		return avgbottleappellation;
	}

	public void setAvgbottleappellation(Double avgbottleappellation) {
		this.avgbottleappellation = avgbottleappellation;
	}

	public Double getDiff_2() {
		return diff_2;
	}

	public void setDiff_2(Double diff_2) {
		this.diff_2 = diff_2;
	}

	public Date getDate_added() {
		return date_added;
	}

	public void setDate_added(Date date_added) {
		this.date_added = date_added;
	}

}
