package nx.winesearch.jsonobjects;

public class WineSearchedObj {
	
	private int idwine;
	private Double avgParkerScore;
	private Double avgAppelationScore;
	private Double diff_1;
	private Double avgBottlePrice;
	private Double avgBottlePriceAppellation;
	private Double diff_2;
	private String currency;
		
	public Double getDiff_1() {
		return diff_1;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public void setDiff_1(Double diff_1) {
		this.diff_1 = diff_1;
	}
	public Double getDiff_2() {
		return diff_2;
	}
	public void setDiff_2(Double diff_2) {
		this.diff_2 = diff_2;
	}
	public Double getAvgParkerScore() {
		return avgParkerScore;
	}
	public void setAvgParkerScore(Double avgParkerScore) {
		this.avgParkerScore = avgParkerScore;
	}
	public Double getAvgAppelationScore() {
		return avgAppelationScore;
	}
	public void setAvgAppelationScore(Double avgAppelationScore) {
		this.avgAppelationScore = avgAppelationScore;
	}
	public Double getAvgBottlePrice() {
		return avgBottlePrice;
	}
	public void setAvgBottlePrice(Double avgBottlePrice) {
		this.avgBottlePrice = avgBottlePrice;
	}
	public Double getAvgBottlePriceAppellation() {
		return avgBottlePriceAppellation;
	}
	public void setAvgBottlePriceAppellation(Double avgBottlePriceAppellation) {
		this.avgBottlePriceAppellation = avgBottlePriceAppellation;
	}
	public int getWine() {
		return idwine;
	}
	public void setWine(int wine) {
		this.idwine = wine;
	}
}
