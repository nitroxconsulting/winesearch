package nx.winesearch.main;

import java.sql.Date;
import java.util.List;

import org.springframework.util.StringUtils;

import nx.winesearch.models.*;
import nx.winesearch.dao.CalibDaoImpl;
import nx.winesearch.dao.CurrencyDaoImpl;
import nx.winesearch.dao.RatingDaoImpl;
import nx.winesearch.dao.ValuationDaoImpl;
import nx.winesearch.dao.WineSearchDaoImpl;
import nx.winesearch.dao.WinesDaoImpl;
import nx.winesearch.jsonobjects.*;

public class WineSearchCheck {
	
	
	  public static WineSearchedObj CheckWinesSearch(String winename, int idccy)
	  {
		  try {
			  boolean debug = false;
			  Double avgPakerScore = 0.0;
			  Double avgScoreAppellation = 0.0;
			  Double avgBotPrice = 0.0;
			  Double avgBotAppellationPrice = 0.0;
			  
			  Double diffScoreAppelation = 0.0;
			  Double diffBottAppelation = 0.0;
			  
			  Double tmpAvgScoreApp = 0.0;
			  Double tmpAvgBottPrice = 0.0;
			  Double tmpAvgBottAppellation = 0.0;
			  
			  Date st = new Date(new java.util.Date().getTime() - 1000 * 60 * 60 * 24 * 7);
			  Date ed = new Date(new java.util.Date().getTime());
			  Date ytd= new Date(new java.util.Date().getTime() - 1000 * 60 * 60 * 24);
			  
			  System.out.println("YTD: "+ytd+ " ST: "+st+" ED: "+ed);
			  						  
			  Wine resultWine = WinesDaoImpl.getWineByName(winename);
			  
			  //Average Parker Score
			  avgPakerScore =RatingDaoImpl.getRating(resultWine.getId());

			  //Average Score by Appellation
			  List<Wine> getWineAppelation = WinesDaoImpl.getWineByAppellation(resultWine.getAppelation().getId());

			  //Traverse each wines result by Appellations
			  for(Wine checkApp : getWineAppelation)
			  {
				  if(!StringUtils.isEmpty(RatingDaoImpl.getRating(checkApp.getId()))){
					  tmpAvgScoreApp += RatingDaoImpl.getRating(checkApp.getId());
				  }
			  }
			  
			  //For Average Bottle Price
			  List<Calib> calibResults = CalibDaoImpl.getSearchedCalib(resultWine.getId(), st, ed);
			  for(Calib checkCalib : calibResults)
			  {
				  if(!StringUtils.isEmpty(ValuationDaoImpl.getAvgBotPrice(checkCalib.getId())))
				  {
					  tmpAvgBottPrice += ValuationDaoImpl.getAvgBotPrice(checkCalib.getId());
				  }
			  }
			  
			  //For Average Bottle Price of the Appellation
			  List<Calib> calibResults2 = CalibDaoImpl.getSearchedCalibByCCY(idccy, ytd);
			  for(Calib checkCalib2 : calibResults2)
			  {
				  if(!StringUtils.isEmpty(ValuationDaoImpl.getAvgBotPrice(checkCalib2.getId())))
				  {
					  tmpAvgBottAppellation += ValuationDaoImpl.getAvgBotPrice(checkCalib2.getId());
				  }
			  }
			  
			  if(calibResults2.size() != 0 && calibResults.size() != 0){
			  			  
				  //[RESULT] average score appelation
				  avgScoreAppellation = tmpAvgScoreApp / getWineAppelation.size();
				  
				  //[RESULT] average bottle price
				  avgBotPrice = tmpAvgBottPrice / calibResults.size();
				  
				  //[RESULT] average bottle price of the appellation
				  avgBotAppellationPrice = tmpAvgBottAppellation / calibResults2.size();
				  
				  // rounding off Values  
				  avgPakerScore = DecimalUtils.round(avgPakerScore, 1);
				  avgScoreAppellation = DecimalUtils.round(avgScoreAppellation, 1);
				  diffScoreAppelation = DecimalUtils.round((avgPakerScore - avgScoreAppellation),1);
				  
				  avgBotPrice = DecimalUtils.round(avgBotPrice, 1);
				  avgBotAppellationPrice = DecimalUtils.round(avgBotAppellationPrice, 1);
				  diffBottAppelation = DecimalUtils.round(avgBotPrice - avgBotAppellationPrice, 1);
				  
				  			  
				  if(debug){
					  System.out.println("average paker score: "+avgPakerScore);
					  System.out.println("average score appelation: "+avgScoreAppellation);
					  System.out.println("difference: "+diffScoreAppelation);
					  System.out.println("average bottle price: "+avgBotPrice);
					  System.out.println("average bottle price fo the appelation: "+avgBotAppellationPrice);
					  System.out.println("Difference bottle price: "+diffBottAppelation);
				  }
			  }
			  
			  System.out.println("Calib Results: ("+calibResults.size() + " " + calibResults2.size() +
					  ") Wineame: " + resultWine.getName() + " Currency: "+ CurrencyDaoImpl.getCurrency(idccy).getCcy()+ " " + ytd);
			  
			  if(!StringUtils.isEmpty(resultWine) && calibResults.size() != 0 && calibResults2.size() != 0){
				  			  
				  WineSearch res = new WineSearch();

				  res.setIdwine(resultWine.getId());
				  res.setAvgParkerScore(avgPakerScore);
				  res.setAvgAppellation(avgScoreAppellation);
				  res.setDiff_1(diffScoreAppelation);
				  res.setAvgbottleprice(avgBotPrice);
				  res.setAvgbottleappellation(avgBotAppellationPrice);
				  res.setDiff_2(diffBottAppelation);
				  res.setCurrency(CurrencyDaoImpl.getCurrency(idccy).getCcy());
				  				  
				  WineSearchDaoImpl.addWineSearch(res);

			  }
			  else{
				  return null;
			  }
			  
		  } catch (Exception e) {
			  e.printStackTrace();
		  }
		  return null;
	  }

}
