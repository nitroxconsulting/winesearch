package nx.winesearch.main;

import helpers.NxFormat;
import info.debatty.java.stringsimilarity.KShingling;
import info.debatty.java.stringsimilarity.StringProfile;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import nx.winesearch.dao.WineParseDaoImpl;
import nx.winesearch.dao.WinesDaoImpl;
import nx.winesearch.models.Wine;
import nx.winesearch.models.WineParse;

public class WineParsing {

	public static String cleanName(String in) {
		
		String out = new String(in).trim();
		out = " " + out;
		out=out.toLowerCase();
		out = out.replaceAll("'", " ");
		out = out.replaceAll(",", " ");
		out = out.replaceAll("-", " ");
		out = NxFormat.stripAccents(out);
		out = out.replaceAll("domaine", "");
		out = out.replaceAll("chateau", "");
		out = out.replaceAll("ch/.", "");
		out = out.replaceAll("ch ", "");
		out = out.replaceAll("Ch ", "");
		out = out.replaceAll("blanc", "");
		out = out.replaceAll("rouge", "");
		out = out.replaceAll("white", "");
		out = out.replaceAll("red", "");
		out = out.replaceAll(" d ", " ");
		out = out.replaceAll(" de ", " ");
		out = out.replaceAll(" du ", " ");
		out = out.replaceAll(" ex ", " ");
		out = out.replaceAll(" en ", " ");
		out = out.replaceAll(" des ", " ");
		out = out.replaceAll(" dos ", " ");
		out = out.replaceAll(" aux ", " ");
		out = out.replaceAll(" le ", " ");
		out = out.replaceAll(" la ", " ");
		out = out.replaceAll(" les ", " ");
		out = out.replaceAll(" dom ", " ");
		out = out.replaceAll(" the ", " ");
		out = out.replaceAll(" igt", " ");
		out = out.replaceAll(" cuvee ", " ");
		out = out.replaceAll("bodegas y vinedos", " ");
		out = out.replaceAll("vino da tavola", " ");
		out = out.replaceAll("tenuta", " ");
		out = out.replace('.', ' ');
		out = out.replaceAll("<br>"," ");

		if (out.contains("gaja"))
			out = out.replaceAll("angelo", "");

		out=out.replaceFirst("Y de ", "Ygrec ");
		out=out.replaceFirst("\"Y\"", "Ygrec ");
		
		return out;
	}
	
	private static WineParse getBestWine(WineParse wine) throws Exception{
		KShingling ks = new KShingling(2);
		StringProfile profile1 = ks.getProfile(cleanName(wine.getParse()));
		
		WineParse similar= new WineParse();
		similar.setScore2(0);
		for(Wine rwine:WinesDaoImpl.getWines()){
			StringProfile profile2 = ks.getProfile(cleanName(rwine.getName()));
			double newh= profile1.cosineSimilarity(profile2) *100;
			if(similar.getScore2()<=newh){
				similar.setPotid2(rwine.getId());
				similar.setPotname2(rwine.getName());
				similar.setScore2(newh);
			}
		}
		return similar;
	}
	
	public static void parse()
	{
			//State Wine Parse "Discarded"
			List<WineParse> discardedWines = WineParseDaoImpl.getWineParseDiscard();
			List<WineParse> discarded = new ArrayList<WineParse>();
			
			//State Wine Parse "New"
			List<WineParse> newWines= WineParseDaoImpl.getWineParseNew(); 
			List<WineParse> newwines= new ArrayList<WineParse>(); 
			
			System.out.println("Total Discarded: "+discardedWines.size());
			for(WineParse wine : discardedWines){
				if(StringUtils.isEmpty(wine.getPotid2()))
				{
					try {
						WineParse similar = getBestWine(wine);
						wine.setPotid2(similar.getPotid2());
						wine.setPotname2(similar.getPotname2());
						wine.setScore2(similar.getScore2());
						WineParseDaoImpl.updateWineParse(wine);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				discarded.add(wine);
			}
					
			System.out.println("Total New: "+newWines.size());
			for(WineParse wine: newWines){
				if(StringUtils.isEmpty(wine.getPotid2()))
				{	
					try{
						
						WineParse similar= getBestWine(wine);
						wine.setPotid2(similar.getPotid2());
						wine.setPotname2(similar.getPotname2());
						wine.setScore2(similar.getScore2());
						WineParseDaoImpl.updateWineParse(wine);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				newwines.add(wine);
			}
				
	}
}
