package nx.winesearch.main;

import java.util.List;
import nx.winesearch.dao.*;
import nx.winesearch.models.Currency;
import nx.winesearch.models.Wine;

public class App {
		
	public static void main(String[] args) throws Exception {
			
		//Hibernate Session
		// Check Configuration file when creating JAR
		AbstractDao.openSession();
				
		List<Wine> w = WinesDaoImpl.getWines();
		System.out.println("Total Wines: "+w.size());
					
		//Selected Currency				
		if(args.length > 0){
			if(args[0].compareToIgnoreCase("winesearch") == 0)
			{
				int[] currency = {47, 44, 149, 56};
				for(int i = 0; i < currency.length; i++)
				{
					for(Wine list : w)
					{
						System.out.print("Wine ID: "+list.getId()+" ");
						WineSearchCheck.CheckWinesSearch(list.getName(), currency[i]);
					}
				}	
			}
			//Wine parsing
			if(args[0].compareToIgnoreCase("wineparse") == 0)
				WineParsing.parse();
			//wine search all currencies
			if(args[0].compareToIgnoreCase("winesearchallcur") == 0)
			{
				//For All Currency
				List<Currency> curr = CurrencyDaoImpl.getCurrencies();
			
				for(Currency ccy : curr)
				{
					for(Wine list : w)
					{
						WineSearchCheck.CheckWinesSearch(list.getName(), CurrencyDaoImpl.getCurrency(ccy.getId()).getId());
					}
				}
			}
			
			if(args[0].compareToIgnoreCase("batch") == 0)
			{
				int[] currency = {47, 44, 149, 56};
				for(int i = 0; i < currency.length; i++)
				{
					for(Wine list : w)
					{
						System.out.print("Wine ID: "+list.getId()+" ");
						WineSearchCheck.CheckWinesSearch(list.getName(), currency[i]);
					}
				}
				WineParsing.parse();
			}
				
		}
	
		AbstractDao.CloseSession();
		System.exit(0);
	}
	
}
